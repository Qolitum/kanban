<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 *
 * Задача
 *
 * class Task
 *
 * @property int        $id             ID
 * @property string     name            Название
 * @property string     project_id      ID проекта
 * @property string     status_id       ID статуса задачи
 * @property string     performer_id    ID исполнителя
 * @property string     priority        Приоритет
 * @property string     description     Описание
 * @property string     deadline        Дедлайн
 *
 */
class Task extends Model
{
    use HasFactory;

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'name',
        'project_id',
        'status_id',
        'performer_id',
        'priority',
        'description',
        'deadline',
    ];

    /**
     * Статус
     *
     * @return belongsTo
     */
    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class, 'id');
    }

    /**
     * Статус
     *
     * @return hasOne
     */
    public function priority(): hasOne
    {
        return $this->hasOne(Priority::class, 'id');
    }

    /**
     * Исполнитель
     *
     * @return belongsTo
     */
    public function performer(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id');
    }
}
