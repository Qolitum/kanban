<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Колонка
 *
 *  Class Project
 *
 * @property int        $id             ID
 * @property string     $board_id       ID доски
 * @property string     $name           Название
 */
class Column extends Model
{
    use HasFactory;

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'board_id',
        'name',
    ];

    /**
     * Статусы
     *
     * @return BelongsToMany
     */
    public function statuses(): BelongsToMany
    {
        return $this->belongsToMany(Status::class, 'column_statuses');
    }

}
