<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;


/**
 *  Доска
 *
 *  class Board
 *
 * @property int    $id                ID
 * @property string name               Название
 * @property string project_id         ID проекта, использующего доску
 */
class Board extends Model
{
    use HasFactory;

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'name',
        'project_id',
    ];

    /**
     * Колонки
     * @return HasMany
     */
    public function columns(): HasMany
    {
        return $this->hasMany(Column::class);
    }
}
