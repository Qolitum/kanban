<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Статус
 *
 * Class Status
 * @package App\Models
 *
 * @property    string      $id         ID
 * @property    string      $name       Название статуса
 */
class Status extends Model
{
    use HasFactory;

    /**
     * @inheritDoc
     */
    protected $primaryKey = 'id';

    /**
     * @inheritDoc
     */
    public $incrementing = false;

    /**
     * @inheritDoc
     */
    protected $keyType = 'string';

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Задачи
     *
     * @return HasMany
     */
    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class);
    }

    /**
     * Статусы
     *
     * @return BelongsToMany
     */
    public function columns(): BelongsToMany
    {
        return $this->belongsToMany(Column::class, 'column_statuses');
    }
}
