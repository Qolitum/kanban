<?php

namespace App\Http\Controllers;

use App\Models\Status;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Validator;

/**
 * Контроллер задач
 */
class TaskController
{
    /**
     * Создать задачу
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        dd($request);
        $request->validate([
            'name'         => 'required',
            'project_id'   => 'required',
            'performer_id' => 'required',
            'priority'     => 'required',
            'status_id'    => 'required',
            'description'  => 'required',
            'deadline'     => 'required',
        ]);
        Task::query()->create([
            'name'         => $request->input('name'),
            'project_id'   => $request->input('project_id'),
            'performer_id' => $request->input('performer_id'),
            'priority'     => $request->input('priority'),
            'status_id'    => $request->input('status_id'),
            'description'  => $request->input('description'),
            'deadline'     => $request->input('deadline'),
        ]);
        return response()->make('create', Response::HTTP_CREATED);
    }
}
