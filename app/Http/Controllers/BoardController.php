<?php

namespace App\Http\Controllers;

use App\Models\Board;
use App\Models\Column;
use App\Models\Project;
use App\Models\Status;
use App\Models\Task;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Контроллер досок
 */
class BoardController
{
    /**
     * Создать доску проекта
     * @param $id
     * @return Response
     */
    public function create($id): Response
    {
        Board::query()
            ->create([
                'name'       => 'Название доски',
                'project_id' => $id,
            ]);
        return response()->make('create', Response::HTTP_CREATED);
    }

    /**
     * Изменить название проекта
     * @param $id
     * @return Response
     */
    public function update(Request $request, $id): Response
    {
        $asd = Board::query()
            ->whereKey($id)
            ->update([
                'name' => $request->input('name'),
            ]);
        return response()->make('update', Response::HTTP_OK);
    }

    /**
     * Удалить доску проекта
     * @param $id
     * @return Response
     */
    public function delete($id): Response
    {
        Board::query()
            ->whereKey($id)
            ->delete();
        return response()->make('', Response::HTTP_NO_CONTENT);
    }
}
