<?php

namespace App\Http\Controllers;

use App\Models\Priority;
use Illuminate\Http\Response;

/**
 * Контроллер приоритетов задач
 */
class PriorityController
{
    /**
     * Получить список приоритетов задач
     * @return Response
     */
    public function getPriorities(): Response
    {
        $priorities = Priority::query()
            ->get(['id', 'name']);
        return response()->make($priorities);
    }
}
