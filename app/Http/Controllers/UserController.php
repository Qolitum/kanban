<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Контроллер пользователей
 */
class UserController
{
    /**
     * Роль пользователя
     */
    const USER_ROLES = [
        'default' => 'Пользователь',
        'admin'   => 'Админ',
    ];

    /**
     * Получить список всех пользователей
     * @return Response
     */
    public function getUsers(): Response
    {
        $users = User::query()
            ->get(['id', 'name', 'surname', 'email', 'role']);
        return response()->make($users);
    }

    /**
     * Создать нового пользователя
     * @param Request $request
     * @return Response
     */
    public function createUser(Request $request): Response
    {
        $request->validate([
            'name'     => 'required',
            'surname'  => 'required',
            'email'    => 'required',
            'password' => 'required',
            'role'     => 'required',
        ]);
        if (!array_key_exists($request->input('role'), self::USER_ROLES)) {
            return response()->make('Выберите роль', Response::HTTP_BAD_REQUEST);
        } else {
            User::query()
                ->create([
                    'name'     => $request->input('name'),
                    'surname'  => $request->input('surname'),
                    'email'    => $request->input('email'),
                    'password' => bcrypt($request->input('password')),
                    'role'     => $request->input('role'),
                ]);
            return response()->make('create', Response::HTTP_CREATED);
        }
    }

    /**
     * Обновить пользователя
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function updateUser(Request $request, User $user): Response
    {
        $request->validate([
            'name'     => 'required',
            'surname'  => 'required',
            'email'    => 'required',
            'role'     => 'required',
        ]);
        $attributes = [
            'name'     => $request->input('name'),
            'surname'  => $request->input('surname'),
            'email'    => $request->input('email'),
            'role'     => $request->input('role'),
        ];
        if ($request->input('password')) {
            $attributes['password'] = bcrypt($request->input('password'));
        }
        $user->update($attributes);
        return response()->make('update', Response::HTTP_CREATED);
    }

    /**
     * Удалить пользователя
     * @param int $id
     * @return Response
     */
    public function deleteUser(int $id): Response
    {
        $user = User::query()->firstWhere('id', '=', $id);
        $user->delete();
        return response()->make('Deleted', Response::HTTP_CREATED);
    }
}