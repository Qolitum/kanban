<?php

namespace App\Http\Controllers;

use App\Models\Status;
use Illuminate\Http\Response;

/**
 * Контроллер исполнителей
 */
class PerformerController
{
    /**
     * Получить исполнителей
     * @return Response
     */
    public function getPerformers(): Response
    {
        $statuses = Status::query()
            ->pluck('name');
        return response()->make($statuses);
    }
}
