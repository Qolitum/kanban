<?php

namespace App\Http\Controllers;

use App\Models\Status;
use Illuminate\Http\Response;

/**
 * Контроллер статусов
 */
class StatusController
{

    /**
     * @return Response
     */
    public function getStatuses(): Response
    {
        $statuses = Status::query()
            ->get(['id', 'name']);
        return response()->make($statuses);
    }
}
