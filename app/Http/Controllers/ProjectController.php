<?php

namespace App\Http\Controllers;

use App\Models\Board;
use App\Models\Column;
use App\Models\Project;
use App\Models\Status;
use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Response;

/**
 * Контроллер проектов
 */
class ProjectController
{
    /**
     * Получить список всех проектов
     * @return Response
     */
    public function getProjects(): Response
    {
        $projects = Project::query()
            ->get();
        return response()->make($projects);
    }

    /**
     * Получить проект
     * @param $id
     * @return Response
     */
    public function getProject($id): Response
    {
        $project = Project::query()
            ->whereKey($id)
            ->with([
                'boards:id,name,project_id',
                'boards.columns:id,board_id,name',
                'boards.columns.statuses:id,name',
                'boards.columns.statuses.tasks:id,name,status_id,performer_id,description,deadline,priority',
            ])
            ->get(['id', 'name', 'description'])
            ->map(fn(Project $project) => [
                'id'     => $project->id,
                'name'   => $project->name,
                'boards' => $project->boards->map(fn(Board $board) => [
                    'id'     => $board->id,
                    'name'   => $board->name,
                    'column' => $board->columns->map(fn(Column $column) => [
                        'id'    => $column->id,
                        'name'  => $column->name,
                        'tasks' => $this->mapTasksByStatusCollection($column->statuses),
                    ]),
                ]),
            ]
            );
        return response()->make($project);
    }

    /**
     * @param Collection<Status> $statuses
     * @return iterable
     */
    private function mapTasksByStatusCollection(Collection $statuses): iterable
    {
        $tasks = [];
        $statuses->each(function (Status $status) use (&$tasks) {
            $pivot = $status->tasks->map(function (Task $task) use ($status) {
                $task->setAttribute('status', [
                    'title' => $status->name,
                    'item' => $task->status_id
                ]);
                return $task->toArray();
            })->toArray();
            $tasks = array_merge($tasks, $pivot);
        });
        return $tasks;
    }
}
