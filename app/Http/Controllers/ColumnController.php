<?php

namespace App\Http\Controllers;

use App\Models\Board;
use App\Models\Column;
use App\Models\Project;
use App\Models\Status;
use App\Models\Task;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Контроллер колонок
 */
class ColumnController
{
    /**
     * Создать колонку
     * @param $id
     * @return Response
     */
    public function create($id): Response
    {
        Column::query()
            ->create([
                'name'     => 'Название колонки',
                'board_id' => $id,
            ]);
        return response()->make('create', Response::HTTP_CREATED);
    }

    /**
     * Обновить колонку
     * @param $id
     * @return Response
     */
    public function update(Request $request, $id): Response
    {
        $request->validate([
            'name'     => 'required|string',
            'statuses' => 'required|array',
        ]);
        $statuses = $request['statuses'];
        $newStatuses = [];
        foreach ($statuses as $status) {
            $status = Status::query()->where('name', $status)->first('id');
            $newStatuses[] = $status['id'];
        };
        $column = Column::query()->with(['statuses'])->whereKey($id);
        $column->update([
            'name' => 'Название колонки',
        ]);
//        $column->statuses()->sync([1, 2]);
        return response()->make('create', Response::HTTP_CREATED);
    }

    /**
     * Удалить колонку
     * @param $id
     * @return Response
     */
    public function delete($id): Response
    {
        Column::query()
            ->whereKey($id)
            ->delete();
        return response()->make('deleted', Response::HTTP_CREATED);
    }

    /**
     * Получить статусы колонки
     * @param $id
     * @return Response
     */
    public function getStatuses($id): Response
    {
        $statuses = Column::query()
            ->whereKey($id)
            ->with([
                'statuses:id,name',
            ])
            ->get(['id', 'name']);
        return response()->make($statuses);
    }

}
