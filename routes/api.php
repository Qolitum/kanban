<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ColumnController;
use App\Http\Controllers\BoardController;
use App\Http\Controllers\PerformerController;
use App\Http\Controllers\PriorityController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::post('auth/login', [AuthController::class, 'login']);
Route::post('auth/logout', [AuthController::class, 'logout']);
Route::post('auth/refresh', [AuthController::class, 'refresh']);
Route::post('auth/me', [AuthController::class, 'me']);

Route::get('/asd', function () {
    return ('welcome');
});

// users section
Route::get('/users', [UserController::class, 'getUsers']);
Route::post('/users/create', [UserController::class, 'createUser']);
Route::put('/users/{user}', [UserController::class, 'updateUser']);
Route::delete('/users/{id}', [UserController::class, 'deleteUser']);

// project section
Route::get('/projects', [ProjectController::class, 'getProjects']);
Route::get('/project/{id}', [ProjectController::class, 'getProject']);

// task section
Route::post('/task/create', [TaskController::class, 'create']);

// performer section
Route::get('/performers', [PerformerController::class, 'getPerformers']);

// priority section
Route::get('/priorities', [PriorityController::class, 'getPriorities']);

// statuses section
Route::get('/statuses', [StatusController::class, 'getStatuses']);

// board section
Route::post('/board/create/{id}', [BoardController::class, 'create']);
Route::put('/board/update/{id}', [BoardController::class, 'update']);
Route::delete('/board/delete/{id}', [BoardController::class, 'delete']);

// Column
Route::post('/column/create/{id}', [ColumnController::class, 'create']);
Route::put('/column/update/{id}', [ColumnController::class, 'update']);
Route::delete('/column/delete/{id}', [ColumnController::class, 'delete']);
Route::get('/column/{id}/statuses', [ColumnController::class, 'getStatuses']);
