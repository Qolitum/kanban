<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id()->comment('ID');
            $table->string('name')->comment('Название задачи');
            $table->integer('project_id')->comment('id проекта на котором используется доска');
            $table->string('status_id')->comment('id статуса задачи');
            $table->string('priority')->comment('Приоритет');
            $table->integer('performer_id')->comment('id исполнителя задачи');
            $table->string('description')->comment('Описание задачи');
            $table->date('deadline')->comment('Дедлайн задачи');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
};
