<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('column_statusфыфвыыфввфыфвывфывыффвывфывфывффвывфы', function (Blueprint $table) {
            $table->id()->comment('ID');
            $table->integer('column_id')->comment('id доски');
            $table->string('status_id')->comment('id статуса');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('column_status');
    }
};
