<?php

namespace Database\Seeders;

use App\Models\Task;
use Illuminate\Database\Seeder;

/**
 * Сидер задач
 *
 * @package Database\Seeders
 */
class TasksSeeder extends Seeder
{
    /**
     * Словарь задач
     */
    private const TASKS = [
        [
            'name'         => 'Первая задача',
            'project_id'   => '1',
            'status_id'    => 'new',
            'priority'     => 'normal',
            'performer_id' => '1',
            'description'  => 'По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст.',
            'deadline'     => '2024-01-15',
        ],
        [
            'name'         => 'Вторая задача',
            'project_id'   => '1',
            'status_id'    => 'new',
            'priority'     => 'normal',
            'performer_id' => '1',
            'description'  => 'По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст.',
            'deadline'     => '2024-01-15',
        ],
        [
            'name'         => 'Третья задача',
            'project_id'   => '1',
            'status_id'    => 'approved',
            'priority'     => 'normal',
            'performer_id' => '1',
            'description'  => 'По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст.',
            'deadline'     => '2024-01-15',
        ],
        [
            'name'         => 'Четверая задача',
            'project_id'   => '1',
            'status_id'    => 'progress',
            'priority'     => 'normal',
            'performer_id' => '1',
            'description'  => 'По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст.',
            'deadline'     => '2024-01-15',
        ],
        [
            'name'         => 'Пятая задача',
            'project_id'   => '1',
            'status_id'    => 'testing',
            'priority'     => 'normal',
            'performer_id' => '1',
            'description'  => 'По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст.',
            'deadline'     => '2024-01-15',
        ],
        [
            'name'         => 'Шестая задача',
            'project_id'   => '1',
            'status_id'    => 'ready',
            'priority'     => 'normal',
            'performer_id' => '1',
            'description'  => 'По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст.',
            'deadline'     => '2024-01-15',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::TASKS as $task) {
            Task::updateOrCreate(([
                'name'         => $task['name'],
                'project_id'   => $task['project_id'],
                'status_id'    => $task['status_id'],
                'priority'     => $task['priority'],
                'performer_id' => $task['performer_id'],
                'description'  => $task['description'],
                'deadline'     => $task['deadline'],
            ]));
        }
    }
}
