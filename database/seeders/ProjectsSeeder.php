<?php

namespace Database\Seeders;

use App\Models\Project;
use Illuminate\Database\Seeder;

/**
 * Сидер проектов
 *
 * @package Database\Seeders
 */
class ProjectsSeeder extends Seeder
{
    /**
     * Словарь пользователей
     */
    private const PROJECTS = [
        [
            'name'        => 'Первый проект',
            'description' => 'Надо сделать быстро',
        ],
        [
            'name'        => 'Второй проект',
            'description' => 'Надо сделать',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::PROJECTS as $project) {
            Project::updateOrCreate(([
                'name'        => $project['name'],
                'description' => $project['description'],
            ]));
        }
    }
}
