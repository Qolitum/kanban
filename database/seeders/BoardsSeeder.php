<?php

namespace Database\Seeders;

use App\Models\Board;
use Illuminate\Database\Seeder;

/**
 * Сидер задач
 *
 * @package Database\Seeders
 */
class BoardsSeeder extends Seeder
{
    /**
     * Словарь задач
     */
    private const BOARDS = [
        [
            'name'         => 'Первая доска',
            'project_id'   => '1',
        ],
        [
            'name'         => 'Вторая доска',
            'project_id'   => '1',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::BOARDS as $board) {
            Board::updateOrCreate(([
                'name'         => $board['name'],
                'project_id'   => $board['project_id'],
            ]));
        }
    }
}
