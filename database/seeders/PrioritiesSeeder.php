<?php

namespace Database\Seeders;

use App\Models\Priority;
use Illuminate\Database\Seeder;

/**
 * Сидер словаря статусов заявок
 *
 * @package Database\Seeders
 */
class PrioritiesSeeder extends Seeder
{
    /**
     * Словарь статусов
     */
    private const PRIORITIES = [
        ['id' => 'low','name' => 'Низкий'],
        ['id' => 'normal','name' => 'Нормальный'],
        ['id' => 'high','name' => 'Высокий'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::PRIORITIES as $priority) {
            Priority::updateOrCreate([
                'id' => $priority['id'],
                'name' => $priority['name']
            ]);
        }
    }
}
