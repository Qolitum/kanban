<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;

/**
 * Сидер словаря статусов заявок
 *
 * @package Database\Seeders
 */
class StatusesSeeder extends Seeder
{
    /**
     * Словарь статусов
     */
    private const STATUSES = [
        ['id' => 'new', 'name' => 'Новая'],
        ['id' => 'approved', 'name' => 'Утвержденная'],
        ['id' => 'progress', 'name' => 'В работе'],
        ['id' => 'testing', 'name' => 'Тестирование'],
        ['id' => 'ready', 'name' => 'Готова'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::STATUSES as $status) {
            Status::updateOrCreate([
                'id' => $status['id'],
                'name' => $status['name']
            ]);
        }
    }
}
