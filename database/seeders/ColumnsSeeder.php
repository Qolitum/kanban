<?php

namespace Database\Seeders;

use App\Models\Column;
use Illuminate\Database\Seeder;

/**
 * Сидер задач
 *
 * @package Database\Seeders
 */
class ColumnsSeeder extends Seeder
{
    /**
     * Словарь задач
     */
    private const BOARDS = [
        [
            'board_id' => '1',
            'name'     => 'Новые задачи',
        ],
        [
            'board_id' => '1',
            'name'     => 'Утвержденные задачи',
        ],
        [
            'board_id' => '2',
            'name'     => 'Задачи в работе',
        ],
        [
            'board_id' => '2',
            'name'     => 'Тестирование',
        ],
        [
            'board_id' => '1',
            'name'     => 'Выполненные задачи',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::BOARDS as $board) {
            Column::updateOrCreate(([
                'name'     => $board['name'],
                'board_id' => $board['board_id'],
            ]));
        }
    }
}
