<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

/**
 * Сидер пользователей
 *
 * @package Database\Seeders
 */
class UsersSeeder extends Seeder
{
    /**
     * Словарь пользователей
     */
    private const USERS = [
        [
            'name'     => 'Георгий',
            'surname'  => 'Милохин',
            'email'    => 'admin@mail.ru',
            'password' => '$2y$10$.sgk7TZEA0tHcegFlKPFRuNH5IxacVPAsXQRKZA2w13q/542Ua66i',
            'role'     => 'admin',
        ],
        [
            'name'     => 'Анатолий',
            'surname'  => 'Петров',
            'email'    => 'petrov@mail.ru',
            'password' => '$2y$10$.sgk7TZEA0tHcegFlKPFRuNH5IxacVPAsXQRKZA2w13q/542Ua66i',
            'role'     => 'default',
        ],
        [
            'name'     => 'Акакий',
            'surname'  => 'Акакиевич',
            'email'    => 'aka@mail.ru',
            'password' => '$2y$10$.sgk7TZEA0tHcegFlKPFRuNH5IxacVPAsXQRKZA2w13q/542Ua66i',
            'role'     => 'default',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::USERS as $user) {
            User::updateOrCreate(([
                'name'     => $user['name'],
                'surname'  => $user['surname'],
                'email'    => $user['email'],
                'password' => bcrypt($user['password']),
                'role'     => $user['role']
            ]));
        }
    }
}
